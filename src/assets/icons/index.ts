import Phone from "./phone.png";
import Location from "./location.png";
import Tachometer from "./tachometer.png";
import PhoneBlue from "./phone-blue.png";
import Search from "./search.png";
import InstagramBlue from "./instagram-blue.png";
import FacebookBlue from "./facebook-blue.png";
import YoutubeBlue from "./youtube-blue.png";
import TwitterBlue from "./twitter-blue.png";
import Right from "./right.png";
import Left from "./left.png";
import Email from "./email.png";
import EmailCircleBlue from "./email-circle-blue.png";
import LocationCircleBlue from "./location-circle-blue.png";
import PhoneCircleBlue from "./phone-circle-blue.png";
import LeftVehicle from './left-vehicle.png';
import RightVehicle from './right-vehicle.png';

export {
  Phone,
  Location,
  Tachometer,
  PhoneBlue,
  Search,
  TwitterBlue,
  FacebookBlue,
  InstagramBlue,
  YoutubeBlue,
  Right,
  Left,
  Email,
  EmailCircleBlue,
  LocationCircleBlue,
  PhoneCircleBlue,
  RightVehicle,
  LeftVehicle
};
