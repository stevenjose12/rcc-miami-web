import Logo from './logo.png';
import Vehicle from './vehicle.jpg';
import Bg from './bg.jpeg';
import WhatsappWhite from './whatsapp-white.png';
import BgAbout from './bg-about.png';
import BgFooter from './bg-footer.jpeg';
import BannerBg from './banner-bg.jpg';
import BannerCar from './banner-car.png';
import About from './about.png';
import Contact from './contact.jpeg';
import BannerAbout1 from './banner-about-1.png';
import BannerAbout2 from './banner-about-2.png';
import BannerAbout3 from './banner-about-3.png';
import BgPopUp from './bg-popup.jpg';
import GalleryBanner from './gallery-banner.jpeg';
import GalleryImage from './gallery-image.png';
import VehicleTest from './vehicle-test.png';
import ReferenceCorners from './reference-corners.png';
import About1 from './about1.jpg';
import About2 from './about2.jpg';
import About3 from './about3.jpg';

export {
	Logo,
	Vehicle,
	WhatsappWhite,
	Bg,
	BgAbout,
	BgFooter,
	BannerBg,
	BannerCar,
	About,
	Contact,
	BannerAbout1,
	BannerAbout2,
	BannerAbout3,
	BgPopUp,
	GalleryBanner,
	GalleryImage,
	VehicleTest,
	ReferenceCorners,
	About1,
	About2,
	About3
}