import { Area } from "models";
import { Action } from "redux";

export interface SetAreasAction extends Action<"Areas/SET"> {
  areas: Area[];
}

export interface PushAreaAction extends Action<"Area/SET"> {
  area: Area;
}

export type RemoveAreaAction = Action<"Area/REMOVE">;

export type AreaAction = SetAreasAction | PushAreaAction | RemoveAreaAction;
export type AreaState = Area[] | null;

export const setAreas = (areas: Area[]): AreaAction => ({
  type: "Areas/SET",
  areas
});

export const pushArea = (area: Area): AreaAction => ({
  type: "Area/SET",
  area
});

export const removeArea = (): AreaAction => ({
  type: "Area/REMOVE"
});
