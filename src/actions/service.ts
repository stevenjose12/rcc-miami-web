import { Service } from "models";
import { Action } from "redux";

export interface SetServicesAction extends Action<"Services/SET"> {
  services: Service[];
}

export type RemoveServicesAction = Action<"Services/REMOVE">;
export type ServicesAction = SetServicesAction | RemoveServicesAction;
export type ServicesState = Service[] | null;

export const setServices = (services: Service[]): ServicesAction => ({
  type: "Services/SET",
  services
});

export const removeServices = (): ServicesAction => ({
  type: "Services/REMOVE"
});
