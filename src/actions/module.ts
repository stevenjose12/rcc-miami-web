import { Module } from "models";
import { Action } from "redux";

export interface SetModulesAction extends Action<"Modules/SET"> {
  modules: Module[];
}

export type RemoveModulesAction = Action<"Modules/REMOVE">;
export type ModulesAction = SetModulesAction | RemoveModulesAction;
export type ModulesState = Module[] | [];

export const setModules = (modules: Module[]): ModulesAction => ({
  type: "Modules/SET",
  modules
});

export const removeModules = (): ModulesAction => ({
  type: "Modules/REMOVE"
});
