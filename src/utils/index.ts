import Colors from "./colors";
import Globals from "./globals";
// import Socket from './socket';
import ENV from "./env";
import axios from "./service";
import dolarService from "./axios";
import Error from "./error";
import Api from "./api";
import Constants from "./constants";
import ROLES from "./RoleId";
import Upload from './upload';

export {
  Colors,
  Globals,
  // Socket,
  ROLES,
  ENV,
  axios,
  dolarService,
  Error,
  Api,
  Constants,
  Upload
};
