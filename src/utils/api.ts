import service from './service';

const getPages = async () => {
    const r = await service.get('page/get-pages');
    if (r?.status >= 200 && r?.status<300) {
        return r.data;
    } else {
        throw r;
    }
};

const updatePage = async (page: any) => {
    const r = await service.post('page/update-enable-page', page);
    if (r?.status>=200 && r?.status<300) {
        return r.data;
    } else {
        throw r;
    }
};

export default {
    getPages,
    updatePage
}
