import axios, { AxiosInstance } from "axios";

const { REACT_APP_BASE_API_URL: baseURL } = process.env;

const service: AxiosInstance = axios.create({baseURL});

export default service;
