import React from "react";
import Popup from "reactjs-popup";
import i18n from "i18next";
import { useDispatch, useSelector } from "react-redux";
import { BgPopUp } from "assets/img";

const PopUp = ({ showPopUp = true }: { showPopUp?: boolean }) => {
  const dispatch = useDispatch();
  const { data } = useSelector((state: { popUp: any }) => state.popUp);
  const _data: any =
    data && data.find((i: any) => i.language.name === i18n.language);

  return (
    <Popup
      open={showPopUp}
      modal
      closeOnDocumentClick
      closeOnEscape
      onClose={() => {
        const existsPopUp = {
          show: false,
          data: data,
        };
        dispatch({
          type: "SET_POPUP",
          payload: existsPopUp,
        });
      }}
    >
      {(close) => (
        <div
          className="popUp col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
          style={{
            backgroundImage: `url(${BgPopUp})`,
          }}
        >
          <span
            className="popUpClose"
            onClick={() => {
              const existsPopUp = {
                show: false,
                data: data,
              };
              dispatch({
                type: "SET_POPUP",
                payload: existsPopUp,
              });
            }}
          >
            X
          </span>
          {_data && (
            <div className="container-text text-center tcol-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <h3>{_data.title}</h3>
              <p>{_data.text}</p>
            </div>
          )}
        </div>
      )}
    </Popup>
  );
};

export default PopUp;
