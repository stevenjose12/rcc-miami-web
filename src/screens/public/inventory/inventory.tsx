import React from "react";
import Layout from "../layout/layout";
import { InventoryService } from "services";
import { Image } from "components";
import { Link, RouteComponentProps } from "react-router-dom";
import { Photo } from "models/photoModel";
import Search from "./search";
import { Globals } from "utils";
import ScrollVehicles from "./scroll-vehicles";
import Loading from "./loading";
import Banner from "./banner";
import { BannerType } from "models/bannerModel";
import queryString from "query-string";

interface Props extends RouteComponentProps<{}, {}, onSearchProps> {}

interface onSearchProps {
  brand?: string | number;
  model?: string | number;
  maxPrice?: string | number;
  minPrice?: string | number;
}

class Inventory extends React.Component<Props> {
  private ref = React.createRef<HTMLDivElement>();

  state: any = {
    loading: true,
    form: {
      minPrice: "",
      maxPrice: "",
      model: "",
      brand: "",
      search: "",
    },
    vehicles: [],
    hasMore: true,
    page: 1,
    total: 0,
  };

  scrollToRef = () => {
    window.scrollTo({
      top: this.ref.current?.offsetTop || 0,
      behavior: "smooth",
    });
  };

  async componentDidMount() {
    const parsed = queryString.parse(this.props.location.search);
    const location: onSearchProps = this.props.location.state;
    if (parsed) {
      await this.setState({
        form: {
          ...this.state.form,
          search: parsed.search,
        },
      });
    }
    if (location) {
      await this.setState({
        form: {
          ...this.state.form,
          brand: location.brand,
          model: location.model,
          maxPrice: location.maxPrice,
          minPrice: location.minPrice,
        },
      });
    }
    this.load();
  }

  load = async (callback = () => {}) => {
    const params = {
      ...this.state.form,
      page: this.state.page,
    };
    const res: any = await InventoryService.get(params);
    this.setState(
      {
        loading: false,
        vehicles: [...this.state.vehicles, ...res.vehicles.items],
        total: res.vehicles.meta.totalItems,
      },
      callback
    );
  };

  onSearch = async (form: onSearchProps) => {
    await this.setState({
      loading: true,
      vehicles: [],
      page: 1,
      form,
    });
    this.load(this.scrollToRef);
  };

  render() {
    const {
      hasMore,
      vehicles,
      total,
      loading,
      brands,
      models,
      form: { brand, model, minPrice, maxPrice, search },
    } = this.state;
    const location = this.props.location.state;

    return (
      <Layout>
        <Banner type={BannerType.INVENTORY} />
        <div id="inventory">
          <Search
            search={queryString.parse(this.props.location.search).search}
            brand={location?.brand}
            model={location?.model}
            minPrice={location?.minPrice}
            maxPrice={location?.maxPrice}
            onSubmit={this.onSearch}
          />
          {loading ? (
            <Loading />
          ) : (
            <div className="container-scroll-vehicles" ref={this.ref}>
              <ScrollVehicles
                height={725}
                vehicles={vehicles}
                changePage={(page: number) => console.log(page)}
                total={total}
                renderLoading={Loading}
                onNext={async () => {
                  const page = this.state.page + 1;
                  await this.setState({
                    page,
                  });
                  this.load();
                }}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

export default Inventory;
