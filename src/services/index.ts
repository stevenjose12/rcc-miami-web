import AuthService from "./modules/authService";
import AreaService from "./modules/areaService";
import ServicesService from "./modules/serviceService";
import UserService from "./modules/userService";
import ProfileService from "./modules/profileService";
import PriceService from "./modules/priceService";
import BrandService from "./modules/brandService";
import PublicService from "./modules/publicService";
import ModelService from "./modules/modelService";
import VehicleService from "./modules/vehicleService";
import InstagramService from "./modules/instagramService";
import AboutService from "./modules/aboutService";
import ContactService from "./modules/contactService";
import InventoryService from "./modules/inventoryService";
import ReportService from "./modules/reportService";
import ModeratorService from "./modules/moderatorService";
import ModuleService from "./modules/moduleService";
import BannerService from './modules/bannerService';
import ScheduleService from './modules/scheduleService';
import CollectionService from './modules/collectionService';

export {
  PublicService,
  PriceService,
  AreaService,
  AuthService,
  ServicesService,
  ProfileService,
  BrandService,
  UserService,
  ModelService,
  VehicleService,
  InstagramService,
  AboutService,
  InventoryService,
  ContactService,
  ReportService,
  ModeratorService,
  ModuleService,
  BannerService,
  ScheduleService,
  CollectionService
};
