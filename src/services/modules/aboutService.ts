import { axios } from "utils";
import { AxiosError, AxiosResponse } from "axios";

class AboutService {
  static get = () => {
    return new Promise((resolve, reject) => {
      axios
        .post('about/get')
        .then(
          (response: AxiosResponse) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  }

  static update = (params = {}) => {
    return new Promise((resolve, reject) => {
      axios
        .post('about/update',params)
        .then(
          (response: AxiosResponse) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  }
}

export default AboutService;
