import { axios, Globals, Error } from "utils";
import { AxiosError, AxiosResponse } from "axios";

class modelService {
  static create = (params: any) => {
    return new Promise<any>((resolve, reject) => {
      axios.post(`models/create`, params)
        .then((res: AxiosResponse) => {
          resolve(res.data);
        })
        .catch((err: AxiosError) => {
          Error.default(err);
          reject(err);
        })
        .finally(Globals.quitLoading);
    });
  };

  static get = (params: any) => {
    return new Promise<any>((resolve, reject) => {
      axios.post(`models/get-all`, params)
        .then((res: AxiosResponse) => {
          resolve(res.data);
        })
        .catch((err: AxiosError) => {
          Error.default(err);
          reject(err);
        })
        .finally(Globals.quitLoading);
    });
  };

  static edit = (params: any) => {
    return new Promise<any>((resolve, reject) => {
      axios.post(`models/edit`, params)
        .then((res: AxiosResponse) => {
          resolve(res.data);
        })
        .catch((err: AxiosError) => {
          Error.default(err);
          reject(err);
        })
        .finally(Globals.quitLoading);
    });
  };
  static delete = (params: any) => {
    return new Promise<any>((resolve, reject) => {
      axios.post(`models/delete`, params)
        .then((res: AxiosResponse) => {
          resolve(res.data);
        })
        .catch((err: AxiosError) => {
          Error.default(err);
          reject(err);
        })
        .finally(Globals.quitLoading);
    });
  };
  static paginate = (page:number, params: any) => {
    return new Promise<any>((resolve, reject) => {
      axios
        .post(`models/get?page=${page}`, params)
        .then((res: AxiosResponse) => {
          resolve(res.data);
        })
        .catch((err: AxiosError) => {
          Error.default(err);
          reject(err);
        })
        .finally(Globals.quitLoading);
    });
  };
}

export default modelService;