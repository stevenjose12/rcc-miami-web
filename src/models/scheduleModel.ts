export type Schedule = {
  id: number,
  day: number,
  start: string,
  end: string,
  open: number
};