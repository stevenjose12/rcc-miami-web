export type Page = {
    id?: number;
    name: string;
    url: string;
    type: number;
    enable: boolean;
    created_at?: string;
    updated_at?: string;
};