import { CollectionPhoto } from "./collectionPhotoModel";

export interface Collection {
  id: number;
  title: string;
  photo: string;
  photos?: CollectionPhoto[];
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
