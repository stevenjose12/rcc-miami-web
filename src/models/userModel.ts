import { Module } from "./moduleModel";

export type User = {
  id: number;
  email: string;
  firstName: string;
  lastName?: string;
  rol: number;
  status: number;
  status_email: number;
  modules: Module[];
  created_at?: string;
  updated_at?: string;
};

export type UserResponse = {
  msg: string;
  user: User;
};

export type UserForm = {
  id?: number | "";
  firstName: string;
  lastName: string;
  email: string;
  password?: string;
  passwordConfirmation?: string;
  status: number;
  module_ids: number[];
};
