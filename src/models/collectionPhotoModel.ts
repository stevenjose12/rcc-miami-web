export interface CollectionPhoto {
  id: number;
  collection_id: number;
  file: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
