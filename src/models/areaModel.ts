import { Service } from "./serviceModel";

export type Area = {
  id: number;
  name: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
};

export interface AreaWithServices extends Area {
  services: Service[];
}

export interface Areas {
  current_page: number;
  last_page: number;
  data: Area[];
}
