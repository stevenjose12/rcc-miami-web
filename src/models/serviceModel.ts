export type Service = {
  id: number;
  area_id: number | string;
  name: string;
  price: number;
  time: number;
  reservations?: number;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
};

export type FormService = {
  id?: number;
  area_id: number | string;
  name: string;
  price: number;
  time: number;
  reservations?: number;
};
