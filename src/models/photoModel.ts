export interface Photo {
  id: number;
  photo: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
