import { Brand } from './brandModel';

export interface Model {
  id: number;
  name: string;
  status: number;
  type: number;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
  brand?: Brand
}
