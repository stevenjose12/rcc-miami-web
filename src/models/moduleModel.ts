export type Module = {
  id: number;
  display_name: string;
  name: string;
  path?: string;
  roles?: Array<any>;
};
