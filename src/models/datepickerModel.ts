export interface CustomProps {
  color?: string | "";
  value?: Date;
  ref: React.RefObject<HTMLDivElement>;
  onClick?(): void;
}
