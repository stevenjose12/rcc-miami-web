export type FormAppointment = {
  id?: number;
  area_id: string;
  date: Date;
  time: Date;
  total_price: number;
  services: Array<any>;
};
