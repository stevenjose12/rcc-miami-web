import { Vehicle } from "./vehicleModel";

export interface Brand {
  id: number;
  name: string;
  status: number;
  type: number;
  vehicles: Vehicle[];
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
