import React from 'react';
import _Viewer from 'react-viewer';

const Viewer = (props: any) => (
	<_Viewer
      visible={ props.visible }
      onClose={ props.onClose }
      drag={ false }
      attribute={ false }
      noNavbar={ false }
      disableMouseZoom={ true }
      zoomSpeed={ 0.4 }
      changeable={ true }
      images={ props.images }
      activeIndex={ props.activeIndex } />
)

export default Viewer;