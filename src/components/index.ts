import PrivateRoute from "./route";
import Button from "./button";
import CardTitle from "./card-title";
import Card from "./card";
import DatePicker from "./datepicker";
import TimePicker from "./timepicker";
import File from "./file";
import Icon from "./icon";
import Input from "./input";
import List from "./list";
import Modal from "./modal";
import Pagination from "./pagination";
import Select from "./select";
import Table from "./table";
import Textarea from "./textarea";
import Title from "./title";
import Tabs from "./tabs";
import Checkbox from "./checkbox";
import PaperClip from "./paperclip";
import InputNumber from "./input-number";
import Image from "./image";
import ImageRegular from "./image-regular";
import Avatar from "./avatar";
import Viewer from './viewer';

export * from "./Loading";
export * from "./CardCustom";

export {
  PrivateRoute,
  InputNumber,
  Button,
  CardTitle,
  Card,
  DatePicker,
  TimePicker,
  File,
  Icon,
  Input,
  List,
  Modal,
  Pagination,
  Select,
  Table,
  Textarea,
  Title,
  Tabs,
  Checkbox,
  PaperClip,
  Image,
  ImageRegular,
  Avatar,
  Viewer
};
