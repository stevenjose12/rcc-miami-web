import React from "react";
import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import { CustomProps } from "models";
import Icon from "./icon";

import es from "date-fns/locale/es";

registerLocale("es", es);
setDefaultLocale("es");

interface TimeProps {
  required?: boolean;
  label?: string;
  color?: string | "";
  value?: Date;
  timeIntervals: number;
  minTime?: Date;
  maxTime?: Date;
  onChange(
    date: Date | null,
    event: React.SyntheticEvent<any> | undefined
  ): void;
}

const CustomInput = React.forwardRef<HTMLDivElement, CustomProps>(
  ({ color, value, onClick }, ref) => (
    <div
      ref={ref}
      className={`container-datepicker ${color ? color : ""}`}
      onClick={onClick}
    >
      <p>{value}</p>
      <Icon name="clock-o" />
    </div>
  )
);

const _TimePicker: React.FC<TimeProps> = ({
  label,
  required = false,
  color,
  minTime,
  maxTime,
  timeIntervals,
  onChange,
  value
}) => (
  <div className="form-group">
    {label && (
      <label className="label-datepicker">
        {label} {required ? <span className="text-danger">(*)</span> : ""}
      </label>
    )}
    <DatePicker
      selected={value}
      onChange={onChange}
      showTimeSelect
      showTimeSelectOnly
      timeIntervals={timeIntervals}
      timeCaption="Hora"
      dateFormat="h:mm aa"
      timeFormat="hh:mm a"
      minTime={minTime}
      maxTime={maxTime}
      customInput={<CustomInput value={value} color={color} />}
    />
  </div>
);

export default _TimePicker;
