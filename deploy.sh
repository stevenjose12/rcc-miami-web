#!/bin/sh
USER=root
HOST=${HOST:-rccmiami.com}

SOURCE=${SOURCE:-build}
TARGET=${TARGET:-/var/www/html/rccmiami/web}
​
echo "Removing existing files..."
ssh $USER@$HOST "rm -ifr $TARGET"
echo ""
echo "Uploading new files..."
echo ""
scp -r "$SOURCE" "$USER@$HOST:$TARGET"
echo ""
echo "Done"
